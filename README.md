# tide-add

The FCOO / Tide-add package is used by FCOO as a practical tool to superpose
predicted tidal currents (TPXO8) on a no-tidal ocean model forecast (ACNFS).

TPXO8 stands for TPXO8 v.1, the OSU Tidal Data Inversion results of Egbert&Erofeeva, http://volkov.oce.orst.edu/tides/tpxo8_atlas.html

ACNFS stands for the operational Arctic Cap Nowcast Forecast System developed by
he US Naval Research Laboratory at Stennis Space Center. Status page: https://www7320.nrlssc.navy.mil/hycomARC/

The package provides two programs extract_fcoo_stennis (to
setup harmonic constituents on a specific grid) and predict_tide_grid (to 
calculate predicted timeseries of gridded values for the tidal elevation and
currents components. There is an accompanying script, sup_tpxo8_ACNFS.py written
in Python which may be used to superpose the gridded timeseries of tides
on a no-tidal ocean model forecast.

Associated input files:
For extract_fcoo_stennis: setup_GRL_ACNFS_Nh.inp, lmsetup_Stennis 
For predict_tide_grid: lat_lon_time_Stennis_Nh

extract_fcoo_stennis is a version of extract_local_model by Gary Egbert & Lana 
Erofeeva re-written to perform 2D array operation over the spherical grid.

predict_tide_grid is an array version of the tidal prediction program by
Egbert&Erofeeva

## Compilation

This is my latest compilation using ifort. Compilation with gfortran is also possible with minor modifications.

Compilation (ifort version 13.1.0):

ifort -c -g -list -O2 -assume nobyterecl -convert big_endian predict_tide_grid.f90 subs_nc.f90

Linking (ifort version 13.1.0):

ifort -O2 -Zp8 -o predict_tide_grid predict_tide_grid.o subs_nc.o

NOTES:

1) A compilation of extract_fcoo_stennis.f90 was experienced to provide an erroneous executable when using an agressive optimization for ifort with option '-O3'. Apparent reason: The compiler overlooks a deeper level in a nested loop "do ic=1,model%nc ..." in subroutine interp_driver_uvq()


Carsten Hansen, 2017-02-02
