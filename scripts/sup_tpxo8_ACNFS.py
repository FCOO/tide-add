#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Python script ''

Testing:

    add_file  = 'TPXO8_ACNFS_GRL_3h_2015061500_v001B.nc'
    var_src  = ['surf_el']
    var_add  = ['z']
    base_file = 'hycom_glb_greenland_2015061500_fcast.nc'
    var_mask  = 'surf_el'
    dest_file = 'sup_test.nc'
    verbose   = True
    export PYTHONPATH=/opt/python2.6/lib64/python2.6/site-packages
    run sup_tpx08_ACNFS.py  -p surf_el,water_u,water_v -a z,u,v -m surf_el --addfile=TPXO8_ACNFS_GRL_3h_2015061500_v001B.nc --basefile=hycom_glb_greenland_2015061500_fcast.nc --destinationfile=sup_test.nc

Purpose:

"""
import numpy as np
from netCDF4 import Dataset
from time import strftime, gmtime, time as wtime
from optparse import OptionParser
from shutil import copy as copyfile


def main():
    # Get command line arguments
    t1 = wtime()
    
    usage = "usage: %prog -p <var1,var2,...> -a <v1,v2,...> -m <var1> --basefile=<filename> --addfile=<filename> --destinationfile=<filename>"
    parser = OptionParser(usage=usage)

    parser.add_option("-b", "--basefile",
                      dest="base_file",metavar="SOURCE",
                      action="store",type="string",default="",
                      help="filename or filename pattern for list of files with field data."\
                          +' Pattern must be inclosed in "<pattern>".')
                      
    parser.add_option("-p", "--basevars",
                      dest="var_src",metavar="VARNAMES",
                      action="store",type="string",default="",
                      help="Names of variables (separated by commas).")
    
    parser.add_option("-a", "--addvars",
                      dest="var_add",metavar="VARNAMESADD",
                      action="store",type="string",default="",
                      help="Names of variables (separated by commas).")

    parser.add_option("-g", "--addfile",
                      dest="add_file",metavar="GRID",
                      action="store",type="string",default="",
                      help="Filename for the grid description of the output grid."\
                          +" This can be any already existing output file.")
                      
    parser.add_option("-m", "--maskvar",
                      dest="var_mask",metavar="MASKNAME",
                      action="store",type="string",default="surf_el",
                      help="Name of mask variable.")

    parser.add_option("-d", "--destinationfile",
                      dest="dest_file",metavar="DESTINATION",
                      action="store",type="string",default="",
                      help="Filename of the output file.")
    
    parser.add_option("-v", "--verbose",
                      dest="verbose",metavar="VERBOSE",
                      action="store_false",default=True,
                      help="Print information during runtime.")

    (options, args) = parser.parse_args()

    base_file = options.base_file
    var_src   = options.var_src.split(',')
    var_add   = options.var_add.split(',')
    add_file  = options.add_file
    var_mask  = options.var_mask
    dest_file = options.dest_file
    # verbose   = not options.verbose
    verbose = True

    #########################################################################

    if not base_file:
        print 'ERROR: Source data file does not seem to exist'
        print 'Base file:      ', options.base_file
        exit()
        
    if verbose:
        print '---------------------------------------------------------------'
        print 'Transform field to be superposed. Extend to masked field.'
        print '---------------------------------------------------------------'
    
    if verbose:
        print  'Copy base file', base_file, 'and open for appending tides' 
    ######################################################################


    # The destination file will be an update of the base file, with values
    # added from the add file to selected fields
        
    copyfile(base_file, dest_file)
    
    fg        = Dataset(dest_file, mode='r+')
    
    lons_out  = fg.variables['lon']
    lats_out  = fg.variables['lat']
    try:
        depth = fg.variables['depth']
    except:
        depth = None
    mask_out  = fg.variables[var_mask]
    time      = fg.variables['time']
    FillValue = mask_out._FillValue
    
    # Create a list of the NetCDF variables to superpose upon
    varl=list()
    for vari in range(len(var_src)):
        varl.append(fg.variables[var_src[vari]])

    var_out = var_src[:]
    
    if verbose:
        print  'Open file to add ', add_file
    ######################################################################
        
    fi       = Dataset(add_file, mode='r')
    lons_src = fi.variables['lon']
    lats_src = fi.variables['lat']
    time_add = fi.variables['time']

    # Hardcoded here: output var names and attributes is a copy of input vars
    vara=list()
    for vari in range(len(var_add)):
        vara.append(fi.variables[var_add[vari]])

        
    if verbose:
        print 'Extrapolate (extend and bind) add data to base(output) mask'

    # Ignore time dim
    if len(mask_out.shape) == 3:
        mask=mask_out[0,:,:].reshape(mask_out.shape[1],-1)
    else:
        mask=mask_out[:,:]

    mask_add=vara[0][0,:,:]

    if verbose:
        print 'Base and add data lat-lon shapes:', mask_out[0][0].shape, mask_add.shape
    
    if not mask_add.shape == mask.shape:
        raise RuntimeError,'The grid shapes of add data ('+mask_add.shape+\
            ') and base data ('+mask.shape+') must be equal'    

    # Determine which cells of mask are missing in mask_add dataset
    # and which neighbour cells in mask_add to extrapolate from, iteratively
    where_get, where_set  = fill_sea_cells(mask_add, mask)

    
    to_unmask = None
    Verbose=verbose
    for i in xrange(len(time)):
        
        if i > 0 and verbose:
            print i, ', ',
        for vari in range(len(vara)):
            vt = vara[vari][i,:,:].copy()
            
            # Iteratively extend v from neighbor sea points to cells masked
            # as land, but where destination shall be sea
            iter = sea_fill(vt, where_get, where_set, decay_base=0.5, \
                                subValue = FillValue,verbose=Verbose)

            
            # Set v = zero where still land but where destination shall be sea,
            # That is, in deep fiords
            if to_unmask is None:
                to_unmask = np.where(vt.mask*(~mask.mask))
                to_mask = np.where(mask.mask)

            if Verbose:
                print 'Correct land/sea mask, timestep', i,\
                      ', clock: %5.2f' % (wtime() - t1),'seconds'

            vt[(to_unmask[0],to_unmask[1])] = 0.
            
            # Fill for land where destination shall be masked
            vt[(to_mask[0],to_mask[1])] = FillValue
            
            # Add the datasets
            # Write array to NetCDF file
            varl[vari][i,:,:] = varl[vari][i,:,:] + vt[:,:]

            if Verbose:
                print 'Fill sea - number of iterations:',iter,'/',len(where_get)
            Verbose=False
        
        if i == 0 and verbose:
            print 'Timesteps: ', i, ', ',
    
    if verbose:
        print 'Clock: %5.2f' % (wtime() - t1),'seconds:'
        print 'Append history information and close output file: ', dest_file

    nowstr = strftime('%Y-%m-%d %H:%M:%S', gmtime())
    hist = "FCOO, %s, sup_tpxo8_ACNFS.py: Data from: %s are remapped and added to the grid of %s\n" %(nowstr, add_file, base_file)
    
    fg.history = hist + fg.history
    fg.close()

    

    #########################################################################

    if verbose:
        print ''
        print '---------------------------------------------------------------'


# Internal routines:
def fill_sea_cells(source,dest,verbose=True):

    if verbose:
        print 'Get land cells to be filled as sea'
        t1 = wtime()

    skn=2
    skni=skn
    sknj=skn#+147

    # Which cells are sea in dest as well as in source
    is_sea = ~(source.mask + dest.mask)

    # Which cells are sea in source
    src_sea = ~source.mask
    
    # Which cells are sea in dest but land in source,
    is_no_sea = ( source.mask > dest.mask )[skni:-skn,sknj:-skn]

    neighborsea_i = np.zeros_like(is_no_sea, dtype=int)
    neighborsea_j = np.zeros_like(is_no_sea, dtype=int)

    num_land = len(np.where(is_no_sea)[0])
    numiter = int(40./skn)

    where_set=[]
    where_get=[]

    def r1(b): return range(-1,-b-1,-1) + range(1,b+1)
    
    for iter in range(numiter):
        for r in range(1,skn):

            seekindx = \
             [ [i,j] for i in [0]     for j in [-r,r]    ] + \
             [ [i,j] for i in [-r,r]  for j in [0]       ] + \
             [ [i,j] for i in [-r,r]  for j in r1(r-1) ] + \
             [ [i,j] for i in r1(r) for j in [-r,r]    ]

            for k in range(len(seekindx)):
                i,j=seekindx[k]
                where_neighbor_sea = \
                    np.where(is_no_sea * src_sea[skni+i:-skn+i,sknj+j:-skn+j])
                
                if len(where_neighbor_sea[0]):
                    neighborsea_i[where_neighbor_sea]=i
                    neighborsea_j[where_neighbor_sea]=j
                    
                    is_no_sea[where_neighbor_sea] = False
                    num_land -= len(where_neighbor_sea[0])
                    if num_land == 0:
                        break
            if num_land == 0:
                break
                # STOP when no missing sea is found
        
        where_neighbor = np.where(~((neighborsea_j==0) * (neighborsea_i==0)))
        where_set.append ( (where_neighbor[0]+skni, where_neighbor[1]+sknj) )
        where_get.append ( (where_neighbor[0]+skni \
                               + neighborsea_i[where_neighbor], \
                            where_neighbor[1]+sknj \
                               + neighborsea_j[where_neighbor]) )

        if num_land == 0:
            break

        src_sea[where_set[iter]]=True
        neighborsea_i[:] = 0
        neighborsea_j[:] = 0
    
    if num_land > 0 and verbose:
        print 'WARNING: Could use more than available iterations: ', numiter,\
            '. Maybe there is a very long fjord?'
    
    if verbose:
        print 'Found land cells to be sea, spend: %5.2f' % (wtime() - t1),\
              'seconds'
    
    return where_get, where_set


def sea_fill(source, where_get, where_set, decay_base=0.5, subValue = -30., rel_precision=0.01, verbose=True):
    """source is a representation (z-, u- or v- component of each constituent
    or a combined prediction) of the interpolated OSU TPXO8-Atlas, and dest
    is an extension of source to the missing cells.

    A first idea is to lower the u- and v-values by a multiplication factor
    decay_base^iter_level, where decay_base is an O(0.5) constant < 1.
    """
    if verbose:
        print 'Do fill the sea cells'
        #t1 = wtime()
        
    decay=decay_base

    num_iter=min( len(where_set), int( np.log(rel_precision)/np.log(decay) ) )
    if verbose:
        print 'last iter: ',(num_iter-1),', index of first cell, lat,lon:',
        print where_get[num_iter-1][0][0], where_get[num_iter-1][1][0]
    
    for iter in range(num_iter):
        #if time is None:
        do_get = np.where( source[(   \
                     where_get[iter][0], where_get[iter][1])] > subValue )
        #else:
        #    do_get = np.where( source[(time,   \
        #                where_get[iter][0], where_get[iter][1])] > subValue )
        
        if len(do_get[0]) == 0 :
            iter-=1
            break
        try:
            get_x = where_get[iter][0][do_get]
            get_y = where_get[iter][1][do_get]
            set_x = where_set[iter][0][do_get]
            set_y = where_set[iter][1][do_get]
            #if time is None:
            set_xy=(set_x,set_y); get_xy=(get_x,get_y)
            #else:
            #    set_xy=(time,set_x,set_y); get_xy=(time,get_x,get_y)
            source[set_xy] = source[get_xy] * decay
        except:
            print 'wrong do_get at iter = %d?'%iter
            print 'source.shape: ', source.shape
            print 'do_get:', do_get
            print 'where_get:', where_get[iter]
            print 'where_set:', where_set[iter]
            print 'source[where_get]:'
            print source[get_xy]
            if len(do_get) > 1:
                continue
            else:
                raise RuntimeError, 'iter=%d'%iter
    
    if verbose:
        print 'Filled sea cells' #, spend: %5.2f' % (wtime() - t1),'seconds'
    
    return iter+1

###########################################################################

# Run main:

if __name__ == '__main__':
   main()
