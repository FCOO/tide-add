#!/usr/bin/env bash

# Purpose: Predict tides using a netcdf, grid version of otps2 Fortran code
# Carsten Hansen, 2015.
# See /home/cha/Projects/Projects/Arctic/OSU_OTPS/OTPS2nc


# CHA, some error handling in consideration:
# See: http://fvue.nl/wiki/Bash:_Error_handling: 
    # Let shell functions inherit ERR trap.  Same as `set -E'.
set -o errtrace 
    # Trigger error when expanding unset variables.  Same as `set -u'.
set -o nounset
# Exit if command fails
set -o errexit

echo $(date)": Executing $0 $1 $2 $3 $4 $5"
echo "Current work directory: cwd=$( pwd )"


executer=$1
input=$2
ep10=$3
stride=$4
provfile=$5

# Read first two lines of the input file
IFS=$'\n'
twolines=($(head -n2 $input))

# Copy name of tidal model control file
echo ${twolines[0]} > input.inp

# Read name lat/lon/time template file and print new file name 'lat_lon_time2'
lat_lon_time_file=${twolines[1]%% *}  #Removes all from the first space in line
echo 'lat_lon_time2' >> input.inp

# Copy lines 3-7
head -n7 $input | tail -n5 >> input.inp

# write provides file name
echo $provfile >> input.inp

# Substitute time info in lat/lon/time template file, output to 'lat_lon_time2'
IFS=$','
striar=($stride)
H0=${striar[0]}
dH=${striar[2]}
Nh=$(((${striar[1]} - $H0)/$dH + 1))

Y_m_d_H=${ep10:0:4}' '${ep10:4:2}' '${ep10:6:2}' '${ep10:8:2}


echo '' > lat_lon_time2
IFS=$'\n'
head -n3 $lat_lon_time_file | while read -r line || [[ -n $line ]]; do
    line=${line/'[Y m d H]'/$Y_m_d_H}
    line=${line/'[Nh]'/$Nh}
    line=${line/'[dH]'/$dH}
    echo $line>>lat_lon_time2
done


# Execute the prediction program

# Just try to add /opt/netcdf-4.2/lib to LD_LIBRARY_PATH

./$executer < input.inp

